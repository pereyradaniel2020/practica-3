﻿using System;

namespace practica_3
{
    class Program
    {
        static void Main(string[] args)
        {
                        //    Crear un programa que pida números positivos al usuario, y vaya calculando la suma de todos ellos (terminará cuando se teclea un número negativo o cero).
            
            int numero, suma = 0;

            do
            {
                Console.Write("coloca un numero ");
                numero = int.Parse(Console.ReadLine());

                if(numero !>0)
                {
                    suma += numero;
                }
                else
                {
                    Console.WriteLine("resultado");
                }

            }while(numero !> 0);

            {
                Console.WriteLine(" La suma de todos los numeros introducidos es {0} ",suma);
            }

        }
    }
}
